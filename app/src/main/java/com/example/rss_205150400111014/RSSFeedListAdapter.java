package com.example.rss_205150400111014;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

public class RSSFeedListAdapter
        extends RecyclerView.Adapter<RSSFeedListAdapter.FeedModelViewHolder> {

    private List<String> mRssFeedModels;

    public static class FeedModelViewHolder extends RecyclerView.ViewHolder {
        private View rssFeedView;

        public FeedModelViewHolder(View v) {
            super(v);
            rssFeedView = v;
        }
    }

    public RSSFeedListAdapter(List<String> rssFeedModels) {
        mRssFeedModels = rssFeedModels;
    }

    public FeedModelViewHolder onCreateViewHolder(ViewGroup parent, int type) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_rss_feed, parent, false);
        FeedModelViewHolder holder = new FeedModelViewHolder(v);
        return holder;
    }

    public void onBindViewHolder(FeedModelViewHolder holder, int position) {
        final String rssFeedModel = mRssFeedModels.get(position);
        ((TextView)holder.rssFeedView.findViewById(R.id.titleText)).setText(rssFeedModel);
    }

    @Override
    public int getItemCount() {
        return mRssFeedModels.size();
    }
}
